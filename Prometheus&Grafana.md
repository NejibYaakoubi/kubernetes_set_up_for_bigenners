# Installation de Helm 3

```bash
# Télécharger le script d'installation Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3

# Rendre le script exécutable
chmod 700 get_helm.sh

# Exécuter le script pour installer Helm 3
./get_helm.sh

# Ajouter le repository stable Helm (optionnel, pour certaines dépendances)
helm repo add stable "https://charts.helm.sh/stable"

# Ajouter le repository Helm de la communauté Prometheus
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

# Ajouter le repository Helm Grafana
helm repo add grafana https://grafana.github.io/helm-charts

# Mettre à jour les repositories Helm
helm repo update

kubectl create namespace monitoring

# Installer Prometheus dans le namespace Monitoring
helm install prometheus prometheus-community/prometheus --namespace monitoring

# Installer Grafana dans le namespace Monitoring
helm install grafana grafana/grafana --namespace monitoring

# Accéder au shell du pod Grafana pour réinitialiser le mot de passe
kubectl exec -it -n monitoring <nom-du-pod-grafana> -- /bin/sh

# Réinitialiser le mot de passe admin de Grafana
grafana-cli admin reset-admin-password <nouveau-mot-de-passe>



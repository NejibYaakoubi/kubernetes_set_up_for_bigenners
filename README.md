# Kubernetes Setup Script

This script automates the setup process for Kubernetes on Ubuntu. It includes the necessary configurations and installations for Docker and Kubernetes tools.

## Prerequisites

Before running the script, ensure that you have the following:

- Ubuntu OS
- Bash shell
- Root or sudo access

## Usage

1. Copy the script content.
2. Open a terminal on your Ubuntu machine.
3. Paste the script into the terminal.
4. Run the script.

```bash
#!/bin/bash

# Disable swap
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# Configure modules for containerd
sudo tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF
sudo modprobe overlay
sudo modprobe br_netfilter

# Configure sysctl for Kubernetes
sudo tee /etc/sysctl.d/k8s.conf <<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
sudo sysctl --system

# Install required packages
sudo apt-get install curl apt-transport-https vim git wget \
  software-properties-common lsb-release ca-certificates -y

# Install containerd
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update && sudo apt-get install -y containerd.io

# Configure containerd
sudo mkdir -p /etc/containerd
sudo containerd config default | sudo tee /etc/containerd/config.toml
sudo sed -e 's/SystemdCgroup = false/SystemdCgroup = true/g' -i /etc/containerd/config.toml 
sudo systemctl restart containerd

# Install Kubernetes tools
sudo apt-get update
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubeadm=1.26.0-00 kubelet=1.26.0-00 kubectl=1.26.0-00

# Prevent automatic updates of Kubernetes tools
sudo apt-mark hold kubelet kubeadm kubectl
# Initialiser le cluster avec kubeadm
`kubeadm init`
```

## Configuration de l'utilisateur

Pour configurer l'accès de l'utilisateur à Kubernetes, suivez ces étapes :

1. **Créer le répertoire de configuration Kubernetes s'il n'existe pas :**

    ```bash
    mkdir -p $HOME/.kube
    ```

2. **Copier la configuration d'administration Kubernetes dans le répertoire de l'utilisateur :**

    ```bash
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    ```

    Assurez-vous de confirmer toute demande de remplacement éventuelle en appuyant sur "y".

3. **Définir la propriété du fichier de configuration Kubernetes sur l'utilisateur actuel :**

    ```bash
    sudo chown $(id -u):$(id -g) $HOME/.kube/config
    ```

Ces étapes garantissent que l'utilisateur a un accès approprié à la configuration de Kubernetes dans son répertoire personnel, facilitant ainsi l'utilisation des outils de gestion de cluster Kubernetes.

